import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Switch, Route, NavLink, Link } from 'react-router-dom'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { logoutUser } from '../../actions/userActions'
import { getProductsToTable, getCollections, getVariants } from '../../actions/productsActions'

import { getDeliveries } from '../../actions/settingsActions'
import { getUsers } from '../../actions/adminActions'
import { getOrders, getEnquries, getOrdersById } from '../../actions/customerActions'
import { filtersOrders } from '../../actions/ordersActions'
import { getStoreProfile } from '../../actions/adminSettings'
import { getSliders, getFAQs } from '../../actions/siteSettings/siteSettings'
import styles from './AdminRoutes.module.css';
import SideBar from './sidebar/index'
import Authenticated from '../misc/auth/Authenticated'
import DashBoard from './dashboard/DashBoard'
import Products from './products/index'
import Orders from './orders/index'
import Categories from './categories/index'
import Users from './users/index'
import Promotions from './promotions/index'
import Settings from './settings/index'
import Customers from './customers/index'
import InfoUser from './InfoUser/InfoUser'
import Contents from './contents/Contents'
import Marketing from './marketing/Marketing'



class AdminRoutes extends Component {


  state = {
    dropdownOpen: false,
    direction: false,
    changeStyle: false
  }




  componentDidMount = () => {
    this.props.getProductsToTable()
    this.props.getDeliveries()
    this.props.filtersOrders({}, 1)
    this.props.getUsers()
    this.props.getStoreProfile()
    this.props.getCollections()
    this.props.getVariants()
    this.props.getSliders()
    this.props.getEnquries()
    this.props.getFAQs()
    this.props.getOrdersById()

  }

  toggle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }



  logoutUser = () => {

    this.props.logoutUser()

  }
  onMouseEnter = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  onMouseLeave = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  
changeWidth = () => {
    this.setState(prevState => ({
      changeStyle: !prevState.changeStyle
    }));
  }
  changeDirection = () => {
    this.setState(prevState => ({
      direction: !prevState.direction
    }));
  }

  renderSettingBar = () => {

    return (

      <Dropdown isOpen={this.state.dropdownOpen}
        toggle={this.toggle}
        onMouseEnter={
          () => {
            setTimeout(() => {
              this.onMouseEnter()
            }, 200);
          }
        }
        onMouseLeave={this.onMouseLeave} >
        <DropdownToggle nav>
          <a className="nav-link nav-link-icon " href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
            <i className="ni ni-circle-08" style={{ fontSize: '36px', color: 'white', }}></i>
            <span className="nav-link-inner--text d-lg-none">User</span>
          </a>
        </DropdownToggle>

        <DropdownMenu>
          <DropdownItem>
            <a className="dropdown-item" href="/admin/infouser">User Informations</a>
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem> <a className="dropdown-item" href="#">Billings</a></DropdownItem>
          <DropdownItem divider />
          <DropdownItem>
            <button
              className="dropdown-item"
              onClick={this.logoutUser}
            >LogOut</button>
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    )
  }

  render() {

    let sidebarWidth = '2';
    let topbarWidth = '10'
    let iconDirection = 'left'

    if (this.state.changeStyle) {
      sidebarWidth = '1';
      topbarWidth = '11'
    }

    if (this.state.direction) {
      iconDirection = 'right'
    }


    return (




      <Fragment>

        <nav className="navbar">
          <div className="container-fluid">
            <div className="row">
              <div className={` col-xl-${sidebarWidth} ${styles.sidebar} fixed-top`}>
                <SideBar
                  changeStyle={this.state.changeStyle}

                />
              </div>

              <div className={`col-xl-${topbarWidth} col-lg-11  ml-auto  fixed-top`}
                style={{
                  backgroundImage: `linear-gradient(120deg, #f6d365 0%, #fda085 100%)`
                }}
              >

                <div className="d-flex">
                  <div className="mr-auto p-3 ">
                    <a
                      style={{ color: '#fff', cursor: 'pointer' }}
                      onClick={
                        () => {
                          this.changeWidth()
                          this.changeDirection()

                        }
                      }
                    >
                      <i className={` ni ni-bold-${iconDirection}`} style={{ fontSize: '25px' }}></i>
                    </a>

                  </div>

                  <div className="ml-auto">
                    <NavLink to="/" className="nav-link mt-2" style={{ color: '#fff' }} target="_blank" >
                      <span className="nav-link-icon d-block ml-2"><i className="ni ni-shop" ></i>Store Front</span>
                    </NavLink>


                  </div>

                  {this.renderSettingBar()}


                </div>
              </div>
            </div>

          </div>
        </nav>

        <section className="py-4 my-4">

          <div className="container-fluid">
            <div className="row">

              <div className={`col-xl-${topbarWidth} col-lg-10 ml-auto`}>
                <Switch>
                  <Route exact path="/admin" render={(props) => (<Products />)} />
                  <Route path="/admin/products" render={(props) => (<Products />)} />
                  <Route path="/admin/orders" render={(props) => (<Orders getOrdersById={this.props.getOrdersById} />)} />
                  <Route path="/admin/users" render={(props) => (<Users />)} />
                  <Route path="/admin/customers" render={(props) => (<Customers />)} />
                  <Route path="/admin/promotions" render={(props) => (<Promotions />)} />
                  <Route path="/admin/settings" render={(props) => (<Settings />)} />
                  <Route path="/admin/infouser" render={(props) => (<InfoUser />)} />
                  <Route path="/admin/contents" render={(props) => (<Contents />)} />
                  <Route path="/admin/marketing" render={(props) => (<Marketing />)} />
                </Switch>

              </div>



            </div>

          </div>


        </section>

      </Fragment >
    )





  }
}





const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
  logoutUser,
  getProductsToTable,
  getDeliveries,
  filtersOrders,
  getUsers,
  getStoreProfile,
  getCollections,
  getVariants,
  getSliders,
  getEnquries,
  getFAQs,
  getOrdersById
}



export default connect(mapStateToProps, mapDispatchToProps)(AdminRoutes);