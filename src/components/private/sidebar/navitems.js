
export const navitems = [

    {
        name: 'inventory',
        linkTo: '/admin/products',
        icon: 'fa-archive',
        admin: false,
        dropdowns: true,
        links: [
            {
                name: 'products',
                linkTo: '/admin/products'
            },
            {
                name: 'collections',
                linkTo: '/admin/products/collections'
            },
            {
                name: 'categories',
                linkTo: '/admin/products/categories'
            },

            {
                name: 'brands',
                linkTo: '/admin/products/brands'
            }


        ]
    },

    {
        name: 'users',
        linkTo: '/admin/users',
        icon: 'fa-users',
        admin: false,
        dropdowns: false
    },
    {
        name: 'customers',
        linkTo: '/admin/customers',
        icon: 'fa fa-address-card',
        admin: false,
        dropdowns: false
    },

    {
        name: 'orders',
        linkTo: '/admin/orders',
        icon: 'fas fa-shopping-bag',
        admin: false,
        dropdowns: false
    },

    {
        name: 'promotions',
        linkTo: '/admin/promotions',
        icon: 'fas fa-bullhorn',
        admin: false,
        dropdowns: false
    },

    {
        name: "settings",
        linkTo: '/admin/settings',
        icon: 'fa-cog',
        admin: false
    },
    {
        name: "contents",
        linkTo: '/admin/contents/headerimgs',
        icon: 'far fa-edit',
        admin: false,
        dropdowns: false
    },
    {
        name: "marketing",
        linkTo: '/admin/marketing',
        icon: 'far fa-ad',
        admin: false,
        dropdowns: false
    }




]